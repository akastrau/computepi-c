#pragma once
#include <random>
#include "Utils.h"

class Runner
{
public:
	void computePi(std::vector<long>& return_value)
	{
		
		for (auto i = 0; i < totalNumberOfPoints;i++)
		{
			x = dist(mt);
			y = dist(mt);
			if (Utils::checkIfPointIsInCircle(x, y)) pointsInCircle++;
		}
		return_value.push_back(pointsInCircle);
	}
	Runner(long int pointsPerThread)
	{
		totalNumberOfPoints = pointsPerThread;
		std::uniform_real_distribution<double> dist(0, 1);
		this->dist = dist;
		this->mt.seed();
	}

	long points_in_circle() const
	{
		return pointsInCircle;
	}

private:
	long int pointsInCircle = 0;
	long long int  totalNumberOfPoints = 0;
	std::mt19937_64 mt;
	std::uniform_real_distribution<double> dist;
	double x = 0.0;
	double y = 0.0;
};
