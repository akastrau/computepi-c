#define _USE_MATH_DEFINES
#include <iostream>
#include <thread>
#include <math.h>
#include <vector>
#include <chrono>
#include "Runner.h"

int main()
{
	std::cout << "\n\tCompute PI - C++ version\n" << std::endl;
	std::cout << "\tNumber of threads: ";

	const long int pointsPerThread = 922337203;
	unsigned concurentThreadsSupported = std::thread::hardware_concurrency();
	std::cout << concurentThreadsSupported << "\n\tTotal number of points: " << pointsPerThread * concurentThreadsSupported << std::endl
		<< std::endl;

	std::vector<std::thread> threads(concurentThreadsSupported);
	std::vector<long int> referenceVector = std::vector<long int>();

	std::cout << "\tStarting threads..." << std::endl;
	auto start = std::chrono::high_resolution_clock::now();

	for (auto i = 0; i != concurentThreadsSupported; i++)
	{
		threads[i] = std::thread(&Runner::computePi, Runner(pointsPerThread), ref(referenceVector));
		std::thread::id id = threads[i].get_id();
		std::cout << "\tStarting thread " << id << std::endl;
	}

	for (auto i = 0; i != concurentThreadsSupported; i++)
	{
		threads[i].join();
	}

	long long int numberOfPointsInCircle = 0;
	long long int totalNumberOfPoints = concurentThreadsSupported * pointsPerThread;
	double pi, error;

	for (auto i = 0; i != concurentThreadsSupported; i++)
	{
		numberOfPointsInCircle += referenceVector[i];
	}

	pi = 4.0 * (static_cast<double>(numberOfPointsInCircle) / static_cast<double>(totalNumberOfPoints));
	error = abs(pi - M_PI) * 100;

	auto end = std::chrono::high_resolution_clock::now();
	using millisecond = std::chrono::duration<float, std::chrono::milliseconds::period>;

	static_assert(std::chrono::treat_as_floating_point<millisecond::rep>::value,
		"Rep required to be floating point");

	auto elapsedTime = millisecond(end - start);

	std::cout << "\n\t\tPI:" << pi << "\n\t\tError: " << error << "%" << std::endl;
	std::cout << "\t\tExecution time: " << elapsedTime.count() / 1000 << "s" << std::endl;

	system("pause");

}
