#pragma once

class Utils
{
public:
	static bool checkIfPointIsInCircle(double x, double y)
	{
		if (pow(x, 2) + pow(y, 2) <= 1) return true;
		return false;
	}
};
